CREATE TABLE roles(
    id CHAR(2) PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE users(
    login VARCHAR(50) PRIMARY KEY,
    name VARCHAR(50),
    surname VARCHAR(50)
);

CREATE TABLE user_roles(
     user_login VARCHAR(50),
     role_id CHAR(2),
     CONSTRAINT user_roles_users_fk FOREIGN KEY(user_login) REFERENCES users(login),
     CONSTRAINT user_roles_roles_fk FOREIGN KEY(role_id) REFERENCES roles(id),
     PRIMARY KEY(user_login,role_id)
);

CREATE TABLE notes(
    id CHAR(36) PRIMARY KEY,
    user_login VARCHAR(50),
    name VARCHAR(50),
    text TEXT,
    CONSTRAINT notes_users_fk FOREIGN KEY(user_login) REFERENCES users(login)
);

INSERT INTO roles(id, name) VALUES
    ('AD','Admin'),
    ('US','User'),
    ('MD','Moderator');
