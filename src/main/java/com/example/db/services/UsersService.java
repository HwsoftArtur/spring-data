package com.example.db.services;

import com.example.db.dto.Page ;
import com.example.db.dto.UserDto;
import com.example.db.entities.User;
import com.example.db.mappers.UserMapper;
import com.example.db.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final static int PAGE_SIZE = 10;


    public Page<UserDto> getPage(Integer pageNum) {

        org.springframework.data.domain.Page<User> userPage
                = userRepository.findAll(PageRequest.of(pageNum-1,PAGE_SIZE));

        List<UserDto> users = userMapper.toDto(userPage.getContent());

        return new Page<>(
                userPage.getTotalPages(),
                PAGE_SIZE,
                pageNum,
                users
        );
    }
}
