package com.example.db.services;

import com.example.db.dto.NoteDto;
import com.example.db.entities.Note;
import com.example.db.entities.User;
import com.example.db.repositories.NotesRepository;
import com.example.db.repositories.UserRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class NotesService {
    private final NotesRepository notesRepository;
    private final UserRepository userRepository;

    @Transactional
    public void create(NoteDto noteDto){
        User u = userRepository
                .findById(noteDto.getUser().getLogin())
                .orElse(null);
        if(u==null) return;

        Note note = new Note();
        note.setName(noteDto.getName());
        note.setText(noteDto.getText());
        note.setUser(u);

        notesRepository.save(note);
    }
}
