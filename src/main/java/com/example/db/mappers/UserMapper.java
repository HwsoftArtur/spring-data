package com.example.db.mappers;

import com.example.db.dto.UserDto;
import com.example.db.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserMapper {
    private final RolesMapper rolesMapper;

    public UserDto toDto(User user){
        return new UserDto()
                .setLogin(user.getLogin())
                .setName(user.getName())
                .setSurname(user.getSurname())
                .setRoles(rolesMapper.toDto(user.getRoles()));
    }

    public List<UserDto> toDto(Collection<User> users){
        return users.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

}
