package com.example.db.mappers;

import com.example.db.dto.RoleDto;
import com.example.db.entities.Role;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RolesMapper {
    public RoleDto toDto(Role role) {
        return new RoleDto()
                .setId(role.getId())
                .setName(role.getName());
    }
    public List<RoleDto> toDto(Collection<Role> roles) {
        return roles.stream().map(this::toDto).collect(Collectors.toList());
    }
}
