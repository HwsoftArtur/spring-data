package com.example.db.repositories;


import com.example.db.entities.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotesRepository extends JpaRepository<Note,String> {

}
