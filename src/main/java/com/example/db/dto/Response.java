package com.example.db.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {
    private Status status;
    private String error;
    private T data;

    public enum  Status {
        OK,FAIL
    }

    public static  <T> Response<T> ok(T data){
        return new Response<T>()
                .setStatus(Status.OK)
                .setData(data);
    }

    public static  <T> Response<T> fail(String error,Class<T> type){
        return new Response<T>()
                .setStatus(Status.FAIL)
                .setError(error);
    }

    public static  Response<?> fail(String error){
        return fail(error, Object.class);
    }
}
