package com.example.db.dto;

import lombok.Data;

@Data
public class NoteDto {
    private UserDto user;
    private String name;
    private String text;
}
