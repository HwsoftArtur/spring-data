package com.example.db.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "roles")
public class Role {
  @Id
  private String id;
  private String name;

  @ManyToMany(mappedBy = "roles")
  List<User> users;

  @Override
  public String toString(){
    return "Role["+id+","+name+"]";
  }
}
