package com.example.db.entities;

import com.example.db.util.UUIDGenerator;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
@Table(name = "notes")
public class Note {
  @Id
  @GenericGenerator(name = UUIDGenerator.NAME, strategy = UUIDGenerator.CLASS)
  @GeneratedValue(generator = UUIDGenerator.NAME)
  private String id;
  private String name;
  private String text;

  @ManyToOne
  @JoinColumn(name = "user_login")
  private User user;
}
