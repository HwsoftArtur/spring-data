package com.example.db.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User {
  @Id
  private String login;
  private String name;
  private String surname;

  @ManyToMany
  @JoinTable(
          name = "user_roles",
          joinColumns = @JoinColumn(name = "user_login"),
          inverseJoinColumns = @JoinColumn(name = "role_id")
  )
  List<Role> roles;
}
