package com.example.db;

import com.example.db.dto.NoteDto;
import com.example.db.dto.Page;
import com.example.db.dto.Response;
import com.example.db.dto.UserDto;
import com.example.db.services.NotesService;
import com.example.db.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UsersController {
    private final UsersService usersService;
    private final NotesService notesService;

    @GetMapping({"{page}",""})
    public Response<Page<UserDto>> getUsers(
            @PathVariable(value = "page", required = false) Integer pageNum
    ){
        if(pageNum==null) pageNum=1;
        Page<UserDto> page = usersService.getPage(pageNum);
        return Response.ok(page);
    }

    @PostMapping("notes")
    public Response<?> add(@RequestBody NoteDto noteDto){
        notesService.create(noteDto);
        return Response.ok(null);
    }


}
